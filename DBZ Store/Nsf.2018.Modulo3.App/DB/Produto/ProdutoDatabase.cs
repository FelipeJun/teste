﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using Nsf._2018.Modulo3.App.DB.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(PedidoDTO curso)
        {
            string script =
            @"INSERT INTO tb_produto (id_curso, nm_produto, vl_preco)  
				            VALUES (@id_curso, @nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_curso", Produto.ID));
            parms.Add(new MySqlParameter("ds_sigla", curso.Sigla));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

    }
}
